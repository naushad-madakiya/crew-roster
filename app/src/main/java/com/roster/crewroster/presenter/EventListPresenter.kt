package com.roster.crewroster.presenter

import com.roster.crewroster.data.Event
import com.roster.crewroster.data.EventRepository
import com.roster.crewroster.data.EventResponse
import com.roster.crewroster.ui.EventListFragmentView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class EventListPresenter(private val view: EventListFragmentView?, private val repository: EventRepository) {
    private val disposable = CompositeDisposable()

    /**
     * fetches events from API/local DB based on Network conditions
     */
    fun loadEvents() {
        disposable.add(repository.getEvents()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { view?.showLoading() }
            .subscribe(
                this::handleSuccessResponse,
                this::handleFailedResponse
            )
        )
    }

    fun clear() {
        disposable.clear()
    }

    /**
     * Handle failure response for Events
     * show error message and hide dialog
     */
    private fun handleFailedResponse(throwable: Throwable) {
        throwable.printStackTrace()
        view?.run {
            hideProgress()
            showMessage(throwable.message)
        }
    }

    /**
     * handle success response for Event
     * convert list to map, show message and hide dialog
     */
    private fun handleSuccessResponse(eventResponse: EventResponse) {
        val eventMap = createMap(eventResponse.localEvents)
        view?.run {
            setEventData(eventMap)
            showMessage(eventResponse.message)
            hideProgress()
        }
    }

    /**
     * create hash map of list event with date as Key and List of Events as Value
     * i.e Hash<String, Event>
     */
    private fun createMap(t: List<Event>?): TreeMap<String, MutableList<Event>> {
        return t?.groupByTo(TreeMap()) { it.date }!!
    }
}
