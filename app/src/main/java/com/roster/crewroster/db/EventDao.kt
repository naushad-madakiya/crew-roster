package com.roster.crewroster.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.roster.crewroster.data.Event


/**
 * Methods to access Event table from database
 *
 * @author Naushad
 */
@Dao
interface EventDao {
    // replace record with new value when unique constraints is failed
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(events: List<Event>)

    @Query("SELECT * from events")
    fun getAll(): List<Event>
}