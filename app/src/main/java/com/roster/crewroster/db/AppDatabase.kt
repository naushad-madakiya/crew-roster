package com.roster.crewroster.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.roster.crewroster.data.Event


/**
 * Database holder for Room Database which is annotated @Database annotation
 * includes the list of entities and abstract methods for DAO
 *
 * @author Naushad
 */
@Database(entities = [Event::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getEventDao(): EventDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun initDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE =
                        Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "crew-roster")
                            .build()
                }
            }
            return INSTANCE
        }
    }
}