package com.roster.crewroster.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.roster.crewroster.util.getFormattedDate
import kotlinx.android.synthetic.main.item_event_header.view.*

class HeaderViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(eventDate: String) {
        view.event_header.text = getFormattedDate(eventDate)
    }
}