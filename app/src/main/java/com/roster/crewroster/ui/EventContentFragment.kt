package com.roster.crewroster.ui


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.roster.crewroster.R
import com.roster.crewroster.data.Event
import com.roster.crewroster.databinding.FragmentContentBinding
import com.roster.crewroster.util.getFormattedDate

/**
 * Fragment for Event Detail View
 */
class EventContentFragment : Fragment() {

    private lateinit var event: Event
    private var listener: EventActivityListener? = null

    companion object {
        private const val EVENT_DATA = "event"
        fun newInstance(event: Event): EventContentFragment {
            val fragment = EventContentFragment()
            val args = Bundle()
            args.putSerializable(EVENT_DATA, event)

            fragment.arguments = args
            return fragment
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EventActivityListener) {
            this.listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        event = arguments?.getSerializable(EVENT_DATA) as Event
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout with binding for this fragment
        val binding: FragmentContentBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_content, container, false)
        binding.event = event

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title = String.format(getString(R.string.string_with_hyphen), event.dutyCode, getFormattedDate(event.date))
        listener?.setTitle(title)
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
