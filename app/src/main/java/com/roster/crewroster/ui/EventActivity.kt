package com.roster.crewroster.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.roster.crewroster.R
import com.roster.crewroster.data.Event
import com.roster.crewroster.util.FragmentUtility
import com.roster.crewroster.util.TransitionType
import kotlinx.android.synthetic.main.activity_event.*


class EventActivity : AppCompatActivity(), EventActivityListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_event)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.framgment_frame, EventListFragment.newInstance())
                .commitNow()
        }
    }

    override fun setTitle(title: String) {
        this.title = title
    }

    override fun showEventContent(event: Event) {
        FragmentUtility.goToFragment(
            supportFragmentManager,
            EventContentFragment.newInstance(event),
            R.id.framgment_frame,
            true,
            TransitionType.SlideHorizontal
        )
    }

}

/**
 * Listener to communicate with EventActivity
 */
interface EventActivityListener {
    fun setTitle(title: String)
    fun showEventContent(event: Event)
}
