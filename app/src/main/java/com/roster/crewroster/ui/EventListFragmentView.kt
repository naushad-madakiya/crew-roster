package com.roster.crewroster.ui

import com.roster.crewroster.data.Event
import java.util.*


/**
 * interface to communicate with EventList activity
 *
 * @author Naushad
 */
interface EventListFragmentView {

    fun showLoading()
    fun hideProgress()
    fun showMessage(message: String?)
    fun setEventData(events: TreeMap<String, MutableList<Event>>)
}