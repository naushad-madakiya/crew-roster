package com.roster.crewroster.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.roster.crewroster.R
import com.roster.crewroster.data.Event
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters
import io.github.luizgrp.sectionedrecyclerviewadapter.StatelessSection

class EventSection(
    private val title: String,
    private val events: List<Event>,
    private val listener: OnEventItemClick
) : StatelessSection(
    SectionParameters
        .builder()
        .itemResourceId(R.layout.item_event)
        .headerResourceId(R.layout.item_event_header)
        .build()
) {
    override fun getContentItemsTotal(): Int {
        return events.size
    }

    override fun getItemViewHolder(view: View): RecyclerView.ViewHolder {
        return EventViewHolder(view)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder !is EventViewHolder) return

        holder.bind(event = events[position], eventListener = listener)
    }

    override fun getHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return HeaderViewHolder(view)
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder) {
        if (holder !is HeaderViewHolder) return

        holder.bind(title)
    }

    interface OnEventItemClick {
        fun onEventClick(event: Event)
    }
}
