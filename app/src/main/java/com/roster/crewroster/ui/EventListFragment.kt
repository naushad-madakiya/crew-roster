package com.roster.crewroster.ui

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ShapeDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.roster.crewroster.R
import com.roster.crewroster.data.Event
import com.roster.crewroster.data.EventRepositoryProvider
import com.roster.crewroster.presenter.EventListPresenter
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_event_list.*
import java.util.*

class EventListFragment : Fragment(), EventListFragmentView, EventSection.OnEventItemClick {
    private lateinit var eventListPresenter: EventListPresenter
    private lateinit var sectionAdapter: SectionedRecyclerViewAdapter
    private lateinit var events: TreeMap<String, MutableList<Event>>

    private var listener: EventActivityListener? = null

    companion object {
        private const val EVENT_LIST_KEY: String = "events_data"
        fun newInstance(): EventListFragment = EventListFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EventActivityListener) {
            listener = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        events = TreeMap()
        eventListPresenter = EventListPresenter(this, EventRepositoryProvider.provideEventRepository())
        sectionAdapter = SectionedRecyclerViewAdapter()
        updatePreviousData(savedInstanceState)
    }

    /**
     * fetch events from savedInstanceState and update tree map
     */
    private fun updatePreviousData(savedInstanceState: Bundle?) {
        if (savedInstanceState != null && savedInstanceState.containsKey(EVENT_LIST_KEY)) {
            events = savedInstanceState.getSerializable(EVENT_LIST_KEY) as TreeMap<String, MutableList<Event>>
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        swiperefresh.setOnRefreshListener { loadEvents() }
        listener?.setTitle(getString(R.string.app_name))
    }

    private fun setupRecyclerView() {
        val divider = DividerItemDecoration(
            activity,
            DividerItemDecoration.VERTICAL
        )

        divider.setDrawable(ShapeDrawable().apply {
            intrinsicHeight = resources.getDimensionPixelOffset(R.dimen.dp_1)
            paint.color = Color.GRAY
        })
        with(event_list) {
            addItemDecoration(divider)
            setHasFixedSize(true)
            adapter = sectionAdapter
        }
    }

    override fun onEventClick(event: Event) {
        listener?.showEventContent(event)
    }


    /**
     * save events bundle to preserve data in local database
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(EVENT_LIST_KEY, events)
    }

    override fun onResume() {
        super.onResume()
        // confirm if events are not received from onSavedInstanceState
        if (events.isEmpty())
            loadEvents()
    }

    override fun onPause() {
        super.onPause()
        eventListPresenter.clear()
    }

    private fun loadEvents() {
        eventListPresenter.loadEvents()
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun setEventData(events: TreeMap<String, MutableList<Event>>) {
        this.events = events
        updateAdapter()
    }

    // update section adapter with latest data
    private fun updateAdapter() {
        sectionAdapter.removeAllSections()
        events.forEach { eventEntry ->
            sectionAdapter.addSection(EventSection(eventEntry.key, eventEntry.value, this))
        }
        sectionAdapter.notifyDataSetChanged()
    }

    override fun showLoading() {
        swiperefresh.isRefreshing = true
    }

    override fun hideProgress() {
        swiperefresh.isRefreshing = false
    }

    override fun showMessage(message: String?) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

}
