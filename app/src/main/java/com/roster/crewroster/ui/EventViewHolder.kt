package com.roster.crewroster.ui

import android.view.View

import androidx.recyclerview.widget.RecyclerView
import com.roster.crewroster.R
import com.roster.crewroster.data.Event
import com.roster.crewroster.util.convertToHHMM
import com.roster.crewroster.util.differenceBetween
import kotlinx.android.synthetic.main.item_event.view.*

/**
 * View holder for Event item
 * set event data according to event duty id
 */
class EventViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(event: Event, eventListener: EventSection.OnEventItemClick) {
        event.run {
            setEventDetails(this)
            view.rootView.setOnClickListener { eventListener.onEventClick(this) }
        }
    }

    private fun setEventDetails(event: Event) {
        when (event.dutyID) {
            "FLT" -> {
                setFlightEvent(event)
            }
            "DO" -> {
                setDayOff(event)
            }
            "OFD" -> {
                setLayover(event)
            }
            "SBY" -> {
                setStandBy(event)
            }
            "POS" -> {
                setPositioning(event)
            }
            else -> throw IllegalArgumentException("Invalid Event -> $event")
        }
    }

    private fun setPositioning(event: Event) {
        view.roster_details.text = event.dutyCode
        setFlightEvent(event)
    }

    private fun setFlightEvent(event: Event) {
        view.run {
            roster_icon.setText(R.string.plane_icon)
            roster_title.text = String.format(
                itemView.context.getString(R.string.string_with_hyphen), event.departure, event.destination
            )
            roster_schedule.text = String.format(
                itemView.context.getString(R.string.string_with_hyphen), event.departureTime, event.arrivalTime
            )
        }
    }

    private fun setDayOff(event: Event) {
        view.run {
            roster_title.text = event.dutyCode
            roster_details.text = event.departure
            roster_icon.setText(R.string.home_icon)
        }
    }

    private fun setLayover(event: Event) {
        view.run {
            roster_title.text = event.dutyCode
            roster_details.text = event.departure
            roster_icon.setText(R.string.briefcase_icon)
            roster_schedule.text = String.format(
                itemView.context.getString(R.string.layouver_time),
                differenceBetween(event.departureTime, event.arrivalTime)
            )
        }
    }

    private fun setStandBy(event: Event) {
        view.run {
            roster_title.text = event.dutyCode
            roster_icon.setText(R.string.standby_icon)
            roster_schedule.text = convertToHHMM(event.arrivalTime)
            roster_details.text =
                String.format(
                    itemView.context.getString(R.string.standby_details), event.dutyID, event.departure
                )
        }
    }
}