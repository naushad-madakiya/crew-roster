package com.roster.crewroster.rest

import com.roster.crewroster.BuildConfig
import com.roster.crewroster.data.Event
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


/**
 * Define rest API calls
 *
 * @author Naushad
 */
interface ApiService {

    /**
     * API request for roster list which will return observable of List<Event>>
     */
    @GET("wp-content/uploads/dummy-response.json")
    fun getRosterList(): Observable<List<Event>>

    /**
     * Factory class for convenient creation of the Api Service interface
     */
    companion object Factory {
        fun create(): ApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}