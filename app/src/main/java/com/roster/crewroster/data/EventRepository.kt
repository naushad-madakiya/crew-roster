package com.roster.crewroster.data

import com.roster.crewroster.db.AppDatabase
import com.roster.crewroster.rest.ApiService
import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.functions.Function
import java.io.IOException

/**
 * Repository implementation for Event data
 * - Fetch data from server
 * - Store data in local database
 * - Return data from local in offline mode
 *
 * @author Naushad
 */
class EventRepository(
    private val apiService: ApiService,
    private val appDataBase: AppDatabase?
) {

    fun getEvents(): Observable<EventResponse> {
        val dao = appDataBase?.getEventDao()

        return apiService.getRosterList()
            .doOnNext { events -> dao?.insertAll(events) }
            .map { events -> EventResponse(events, "List Updated") }
            .onErrorResumeNext(Function {
                handleEventApiError(it)
            })
    }

    /**
     * creates Observable source when exception occurs
     * If IO exception occurs, return data from local database
     * Else, forward the exception as it is
     */
    private fun handleEventApiError(throwable: Throwable): ObservableSource<EventResponse> {
        return ObservableSource { observer ->
            if (throwable is IOException) {
                observer.onNext(
                    EventResponse(getLocalEvents(), "Network Error, Loading data from Local Database")
                )
            } else
                observer.onError(throwable)
        }
    }

    /**
     * fetch All events from local database
     */
    private fun getLocalEvents(): List<Event>? {
        val dao = appDataBase?.getEventDao()

        return dao?.getAll()
    }
}
