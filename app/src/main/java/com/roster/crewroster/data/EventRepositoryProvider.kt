package com.roster.crewroster.data

import com.roster.crewroster.db.AppDatabase
import com.roster.crewroster.rest.ApiService


/**
 * Provider for EventRepositoryProvider
 *
 * @author Naushad
 */
object EventRepositoryProvider {
    fun provideEventRepository(): EventRepository {
        return EventRepository(ApiService.create(), AppDatabase.INSTANCE)
    }
}