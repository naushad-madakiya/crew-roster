package com.roster.crewroster.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * POJO for event data
 * act as Response and Database entity for room
 *
 * use composite primary keys as record doesn't have single unique field
 */
@Entity(tableName = "events", primaryKeys = ["date", "departure", "destination"])
data class Event(
    @ColumnInfo @SerializedName("Flightnr") val flightnr: String,
    @ColumnInfo @SerializedName("Date") val date: String,
    @ColumnInfo @SerializedName("Aircraft Type") val aircraftType: String,
    @ColumnInfo @SerializedName("Tail") val tail: String,
    @ColumnInfo @SerializedName("Departure") val departure: String,
    @ColumnInfo @SerializedName("Destination") val destination: String,
    @ColumnInfo @SerializedName("Time_Depart") val departureTime: String,
    @ColumnInfo @SerializedName("Time_Arrive") val arrivalTime: String,
    @ColumnInfo @SerializedName("DutyID") val dutyID: String,
    @ColumnInfo @SerializedName("DutyCode") val dutyCode: String,
    @ColumnInfo @SerializedName("Captain") val captain: String,
    @ColumnInfo @SerializedName("First Officer") val firstOfficer: String,
    @ColumnInfo @SerializedName("Flight Attendant") val flightAttendant: String
) : Serializable {

    fun isFlightEvent(): Boolean = dutyID == "FLT"
    fun isStandBy(): Boolean = dutyID == "SBY"
    fun isPositioning(): Boolean = dutyID == "POS"
    fun isLayoverEvent(): Boolean = dutyID == "OFD"
}

data class EventResponse(val localEvents: List<Event>?, val message: String)