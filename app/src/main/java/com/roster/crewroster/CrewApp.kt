package com.roster.crewroster

import android.app.Application
import com.roster.crewroster.db.AppDatabase
import com.roster.crewroster.util.ReleaseTree
import timber.log.Timber


/**
 * Application class of CrewApp

 * @author Naushad
 */
class CrewApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(if (BuildConfig.DEBUG) Timber.DebugTree() else ReleaseTree())

        // Initialize database for once
        AppDatabase.initDatabase(this)
    }
}