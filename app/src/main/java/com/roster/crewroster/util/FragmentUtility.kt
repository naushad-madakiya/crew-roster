package com.roster.crewroster.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.roster.crewroster.R


/**
 * https://github.com/myotive/blog-fragments-2017/blob/master/fragment_sample_mvp/src/main/java/com/example/fragment_sample_mvp/fragments/RepositoryFragment.java#L126
 *
 * @author Naushad
 */

object FragmentUtility {

    fun goToFragment(
        fragmentManager: FragmentManager,
        fragment: Fragment,
        containerId: Int,
        addToBackstack: Boolean,
        transitionType: TransitionType
    ) {
        var enter = 0
        var exit = 0
        var popEnter = 0
        var popExit = 0
        when (transitionType) {
            TransitionType.SlideHorizontal -> {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
            TransitionType.SlideVertical -> {
                enter = R.anim.slide_in_bottom
                exit = R.anim.slide_out_bottom
                popEnter = R.anim.slide_in_bottom
                popExit = R.anim.slide_out_bottom
            }
        }

        val ft = fragmentManager.beginTransaction()

        val tag = fragment::class.java.simpleName

        if (addToBackstack) {
            ft.addToBackStack(tag)
        }

        ft.setCustomAnimations(enter, exit, popEnter, popExit)
            .replace(containerId, fragment, tag)
            .commit()
    }
}

enum class TransitionType {
    SlideHorizontal,
    SlideVertical
}