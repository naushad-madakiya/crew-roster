package com.roster.crewroster.util

import timber.log.Timber

/**
 * Timber custom tree implementation for release builds without any logs printing
 */
class ReleaseTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}