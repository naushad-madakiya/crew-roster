package com.roster.crewroster.util

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Common Date and Time utility functions for Crew Roster App
 *
 * @author Naushad
 */

/**
 * receive date in dd/MM/yyyy format and convert it to EE dd MMM yyy format with dutch local
 *
 * 01/01/2019 -> tue 01 jan 2019
 */
fun getFormattedDate(date: String): String {
    val locale = Locale("NL", "nl")
    val sdf: Date = SimpleDateFormat("dd/MM/yyyy", locale).parse(date)
    val expectedSdf = SimpleDateFormat("EEE dd MMM yyyy", locale)

    return expectedSdf.format(sdf).toString()
}

/**
 * convert HH:mm:ss to HH:mm format
 *
 * 23:12:66 -> 23:12
 */
fun convertToHHMM(time: String): String {
    val sdf: Date = SimpleDateFormat("HH:mm:ss", Locale.ENGLISH).parse(time)
    val expectedSdf = SimpleDateFormat("HH:mm", Locale.ENGLISH)

    return expectedSdf.format(sdf).toString()
}

/**
 * calculate difference between 2 timings
 * return HH:mm -> "hour:minute"
 */
fun differenceBetween(startTime: String, endTime: String): String {
    val format = SimpleDateFormat("HH:mm", Locale.ENGLISH)
    val date1 = format.parse(startTime)
    val date2 = format.parse(endTime)
    val difference = date2.time - date1.time

    return StringBuilder()
        .append(TimeUnit.MILLISECONDS.toHours(difference))
        .append(":")
        .append(TimeUnit.MILLISECONDS.toMinutes(difference) % TimeUnit.HOURS.toMinutes(1))
        .toString()
}