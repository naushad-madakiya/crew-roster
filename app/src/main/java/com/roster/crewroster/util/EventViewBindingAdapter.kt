package com.roster.crewroster.util

import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.roster.crewroster.R
import com.roster.crewroster.data.Event

@BindingAdapter("event_duration")
fun setEventDuration(textView: AppCompatTextView, event: Event) {
    val time: String = when {
        event.isLayoverEvent() -> {
            val actualTime: String = String.format(
                textView.context.getString(R.string.string_with_hyphen), event.departureTime, event.arrivalTime
            )
            val difference: String = String.format(
                textView.context.getString(R.string.layouver_time),
                differenceBetween(event.departureTime, event.arrivalTime)
            )

            "$actualTime ($difference)"
        }
        event.isStandBy() -> convertToHHMM(event.arrivalTime)
        else -> ""
    }

    textView.text = time
}
