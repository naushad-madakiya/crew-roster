package com.roster.crewroster.data

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.roster.crewroster.db.AppDatabase
import com.roster.crewroster.db.EventDao
import org.hamcrest.Matchers.equalTo
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class EventEntityReadWriteTest {
    private lateinit var eventDao: EventDao
    private lateinit var db: AppDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        eventDao = db.getEventDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeAndReadEvents() {
        // GIVEN events are available in local database
        val events: List<Event> = TestUtil.createEvents(5)
        eventDao.insertAll(events)

        // WHEN all events are retrieved from local database
        val updatedEvents = eventDao.getAll()

        // THEN returned events should be same as inserted
        Assert.assertThat(updatedEvents, equalTo(events))
    }
}