package com.roster.crewroster.data

object TestUtil {
    fun createEvents(count: Int): List<Event> {
        return (0 until count).map {
            createEvent(
                "flightnr$it",
                "date$it",
                "aircraftType$it",
                "tail$it",
                "departure$it",
                "destination$it",
                "timeDepart$it",
                "timeArrive$it",
                "dutyId$it",
                "dutyCode$it",
                "captain$it",
                "firstOfficer$it",
                "flightAttendant$it"
            )
        }
    }

    private fun createEvent(
        flightnr: String, date: String, aircraftType: String, tail: String, departure: String,
        destination: String, timeDepart: String, timeArrive: String, dutyId: String,
        dutyCode: String, captain: String, firstOfficer: String, flightAttendant: String
    ): Event = Event(
        flightnr,
        date,
        aircraftType,
        tail,
        departure,
        destination,
        timeDepart,
        timeArrive,
        dutyId,
        dutyCode,
        captain,
        firstOfficer,
        flightAttendant
    )
}
