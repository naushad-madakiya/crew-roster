## Roster Buster - Crew Roster 

App display user’s roster with monthly planning with specific duties.

###  Features
- Display roster of events
- Show details of specific event
- Swipe to refresh on list screen
- Use local database for offline support


### Local Database
- [Android Room](https://developer.android.com/training/data-storage/room/)
- Version: 2.1.0-alpha04

## Libraries
- [Retrofit](https://square.github.io/retrofit/) - 2.5.0
- [SectionedRecyclerViewAdapter](https://github.com/luizgrp/SectionedRecyclerViewAdapter) - 1.2.0
- [Data Binding](https://developer.android.com/topic/libraries/data-binding/)